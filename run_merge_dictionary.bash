# 06 Jun 2019
# By: Aldrian Obaja Muis
# Merge Tok Pisin dictionaries from various sources

python3 merge_dictionary.py \
    --inpaths lexicon_tpi_mendelexicon.tsv \
              lexicon_tpi_oksapmin.tsv \
              lexicon_tpi_tokpisin_com.tsv \
              lexicon_tpi_wiktionary.tsv \
    --outpath _lexicon_tpi_all.tsv
cat _lexicon_tpi_all.tsv | sed -e 's/^[Tt]o //g' | LC_COLLATE=C sort > lexicon_tpi_all.tsv
rm -f _lexicon_tpi_all.tsv
